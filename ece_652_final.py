import sys
from math import gcd
from functools import reduce
from fractions import Fraction
import heapq

# Extracting tasks from workload.txt
def read_workload(workload):
    tasks = []
    with open(workload, 'r') as file:
        for line in file:
            line = line.strip()
            if line: 
                e, p, d = map(lambda x: round(float(x), 3), line.split(','))
                tasks.append((e, p, d))
    return tasks

# LCM of two numbers
def lcm(a, b):
    return abs(a * b) // gcd(a, b)

# LCM of multiple numbers
def lcm_multiple(numbers):
    return reduce(lcm, numbers)

# LCM of floating numbers
def lcm_of_floats(numbers):
    fractions = [Fraction(num).limit_denominator() for num in numbers]
    common_denominator = lcm_multiple([fraction.denominator for fraction in fractions])
    integers = [int(fraction * common_denominator) for fraction in fractions]
    lcm_of_integers = lcm_multiple(integers)
    lcm_of_floats = lcm_of_integers / common_denominator
    return lcm_of_floats

# Hyperperiod of taskset
def hyperperiod(tasks):
    periods = [task[1] for task in tasks]
    hp = lcm_of_floats(periods)
    return round(hp, 3)

# Utilization of workload
def utilization(tasks):
    return round(sum(e / p for e, p, _ in tasks), 3)

# Deadline Monotonic Scheduler
def deadline_monotonic_schedule(tasks, hyperperiod):
    time = 0
    task_queue = []
    wait_queue = []
    current_task = None
    preemptions = [0]*len(tasks)

    for idx, task in enumerate(tasks):
        e, p, d = task
        wait_queue.append((p, e, d, idx, d))
    
    while round(time, 3) < hyperperiod:
        for task in wait_queue:
            period, execution, deadline, task_idx, priority = task
            if round(time, 3) % period == 0:
                heapq.heappush(task_queue, (priority, deadline, execution, period, execution, task_idx))

        wait_queue = [task for task in wait_queue if round(time, 3) % task[0] != 0]

        if task_queue:
            priority, deadline, execution, period, remaining_time, task_idx = heapq.heappop(task_queue)
            if current_task is not None and current_task != task_idx:
                preemptions[current_task] += 1
            current_task = task_idx
            if remaining_time > 0:
                remaining_time = round(remaining_time - 0.001, 3)
                if remaining_time > 0:
                    heapq.heappush(task_queue, (priority, deadline, execution, period, remaining_time, task_idx))
                else:
                    current_task = None
                    wait_queue.append((period, execution, round(deadline + period, 3), task_idx, priority))
        
        time = round(time + 0.001, 3)

    schedulable = len(wait_queue) == len(tasks)
    return schedulable, preemptions

# Main Function
def main(workload):
    tasks = read_workload(workload)
    hp = hyperperiod(tasks)
    u = utilization(tasks)
    
    if u <= 1:
        schedulable, preemptions = deadline_monotonic_schedule(tasks, hp)
    else:
        schedulable = False

    if schedulable:
        print(1)
        print(','.join(map(str, preemptions)))
    else:
        print(0)
        print('')

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit(1)

    input_file = sys.argv[1]
    main(input_file)

